package cn.hserver.dns;

import cn.hserver.HServerApplication;
import cn.hserver.core.interfaces.InitRunner;
import cn.hserver.core.ioc.annotation.Bean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Bean
public class DnsInit implements InitRunner {
    private static final Logger log = LoggerFactory.getLogger(DnsInit.class);

    @Override
    public void init(String[] strings) {
       new Thread(()->{
           log.info("启动DNS服务");
           DnsServer dnsServer = new DnsServer();
           dnsServer.listenAndRun();
       }).start();
    }
}
