package cn.hserver.dns.cache;

import cn.hserver.dns.bean.DnsBean;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class DnsCache {

    private static final Cache<String, List<DnsBean>> cache = Caffeine.newBuilder()
            .expireAfterWrite(3, TimeUnit.MINUTES)
            .maximumSize(5000)
            .build();

    public static synchronized List<DnsBean> getDomain(String domain) {
        return cache.getIfPresent(domain);
    }

    public static synchronized void put(String domain, List<DnsBean> dnsBeans) {
        cache.put(domain, dnsBeans);
    }
}
