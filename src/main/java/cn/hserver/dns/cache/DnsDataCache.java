package cn.hserver.dns.cache;

import cn.hserver.core.server.context.ConstConfig;
import cn.hserver.dns.bean.DnsData;
import cn.hserver.dns.util.Sort;
import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson2.JSON;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DnsDataCache {

    public static String ROOT_PATH = ConstConfig.PATH + "data" + File.separator;

    public static String dateFile() {
        LocalDate now = LocalDate.now();
        return now.getYear() + "-" + now.getMonthValue() + "-" + now.getDayOfMonth() + ".txt";
    }

    public static void put(DnsData dnsData) {
        try {
            FileUtil.appendString(JSON.toJSONString(dnsData) + "\n", new File(ROOT_PATH + dateFile()), StandardCharsets.UTF_8);
        } catch (Exception ignored) {
        }
    }


    public static Map<String, Integer> dataGroup(String file) {
        List<String> strings = FileUtil.readLines(new File(ROOT_PATH + file), StandardCharsets.UTF_8);
        HashMap<String, Integer> data = new HashMap<>();
        for (String string : strings) {
            DnsData dnsData = JSON.parseObject(string, DnsData.class);
            Integer integer = data.get(dnsData.getDomain());
            if (integer == null) {
                integer = 0;
            }
            integer++;
            data.put(dnsData.getDomain(), integer);
        }
        return Sort.sortAscend(data);
    }

}
