package cn.hserver.dns;

import cn.hserver.HServerApplication;
import cn.hserver.core.ioc.annotation.HServerBoot;

@HServerBoot
public class StartWeb {
    public static void main(String[] args) {
        HServerApplication.run(StartWeb.class, 10110, args);
    }
}
