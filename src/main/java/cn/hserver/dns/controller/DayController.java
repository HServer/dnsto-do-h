package cn.hserver.dns.controller;

import cn.hserver.dns.cache.DnsDataCache;
import cn.hserver.plugin.web.annotation.Controller;
import cn.hserver.plugin.web.annotation.GET;
import cn.hserver.plugin.web.interfaces.HttpResponse;

import java.io.File;
import java.util.HashMap;

@Controller
public class DayController {

    @GET("/day")
    public void day(HttpResponse response, String file) {
        if (file == null) {
            file = DnsDataCache.dateFile();
        }
        HashMap<String, Object> data = new HashMap<>();
        data.put("days", new File(DnsDataCache.ROOT_PATH).list());
        data.put("data", DnsDataCache.dataGroup(file));
        response.sendTemplate("./day.ftl", data);
    }
}
