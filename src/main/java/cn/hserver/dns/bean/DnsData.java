package cn.hserver.dns.bean;

import com.alibaba.fastjson2.annotation.JSONField;

import java.util.Date;

public class DnsData {
    /**
     * 创建时间
     */
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date date;

    /**
     * 查询域名
     */
    private String domain;

    /**
     * 查询类型
     */
    private String type;

    /**
     * DNS 查询数据
     */
    private String data;

    /**
     * 耗时
     */
    private Long time;

    public DnsData() {
    }

    public DnsData(String domain, String type, String data, Long time) {
        this.date = new Date();
        this.domain = domain;
        this.type = type;
        this.data = data;
        this.time = time;
    }

    @Override
    public String toString() {
        return "DnsData{" +
                "date=" + date +
                ", domain='" + domain + '\'' +
                ", type='" + type + '\'' +
                ", data='" + data + '\'' +
                ", time=" + time +
                '}';
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
