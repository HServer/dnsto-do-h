package cn.hserver.dns.bean;

public class DnsBean {

    private String name;

    private Integer TTL;
    /**
     * A	1	IPv4 地址	101.37.183.171
     * NS	2	NS 记录	ns1.taobao.com.
     * CNAME	5	域名 CNAME 记录	www.taobao.com.danuoyi.tbcache.com.
     * SOA	6	ZONE 的 SOA 记录	ns4.taobao.com. hostmaster.alibabadns.com. 2018011109 3600 1200 3600 360
     * TXT	16	TXT 记录	"v=spf1 include:spf1.staff.mail.aliyun.com -all"
     * AAAA	28	IPv6 地址	240e:e1:f300:1:3::3fa
     */
    private Integer type;

    private String data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTTL() {
        return TTL;
    }

    public void setTTL(Integer TTL) {
        this.TTL = TTL;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "DnsBean{" +
                "name='" + name + '\'' +
                ", TTL=" + TTL +
                ", type=" + type +
                ", data='" + data + '\'' +
                '}';
    }
}
