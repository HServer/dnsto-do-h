<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%">
<head>
    <meta charset="utf-8">
</head>
<body style="height: 100%; margin: 0">

<div style="display: flex;
    justify-content: center;">
    <#list days as time>
        <div style="text-align: center; padding: 20px">
            <a href="/day?file=${time}">${time}</a>
        </div>
    </#list>
</div>

<div id="container" style="height: 100%">

</div>
<script type="text/javascript" src="https://fastly.jsdelivr.net/npm/echarts@5.4.1/dist/echarts.min.js"></script>
<script type="text/javascript">
    var dom = document.getElementById('container');
    var myChart = echarts.init(dom, null, {
        renderer: 'canvas',
        useDirtyRect: false
    });
    var app = {};

    var option;

    option = {
        dataZoom: [
            {
                type: "slider", //滑动条
                show: true, //开启
                yAxisIndex: [0],
                left: "96%", //滑动条位置
                start: 0, //初始化时，滑动条宽度开始标度
                end: 20, //初始化时，滑动条宽度结束标度<br>                   },
            },
            {
                type: "inside", //内置滑动，随鼠标滚轮展示
                yAxisIndex: [0],
                start: 20, //初始化时，滑动条宽度开始标度
            },
        ],
        dataset: {
            source: [
                ['amount', 'product'],
                <#list data?keys as key>
                [${data[key]}, "${key}->${data[key]}"],
                </#list>
            ]
        },
        grid: {containLabel: true},
        xAxis: {name: 'amount'},
        yAxis: {type: 'category'},
        visualMap: {
            orient: 'horizontal',
            left: 'center',
            min: 10,
            max: 100,
            text: ['High Score', 'Low Score'],
            dimension: 0,
            inRange: {
                color: ['#65B581', '#FFCE34', '#FD665F']
            }
        },
        series: [
            {
                type: 'bar',
                encode: {
                    // Map the "amount" column to X axis.
                    x: 'amount',
                    // Map the "product" column to Y axis
                    y: 'product'
                }
            }
        ]
    };

    if (option && typeof option === 'object') {
        myChart.setOption(option);
    }

    window.addEventListener('resize', myChart.resize);
</script>
</body>
</html>